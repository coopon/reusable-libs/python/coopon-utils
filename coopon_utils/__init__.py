def remove_fields(model, fields):
    '''remove fields from model and return model
    Args:
        model (dict): dict representing some model or schema
        fields (iterable): a list of tuple of fields to remove
    Returns:
        model dict with fields removed
    '''
    [model.pop(field, None) for field in fields]
    return model
