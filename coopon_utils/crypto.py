import bcrypt
import jwt

from datetime import datetime

from base64 import b64encode
from uuid import uuid4

def generate_unique_id():
    return b64encode(uuid4().bytes).decode().replace('=','')

def get_password_salt(password):
    return bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

def verify_password(plain_password, salted_password):
    return bcrypt.checkpw(plain_password.encode(), salted_password.encode())

def get_auth_token(data, secret, expire_at, algo='HS256'):
    try:
        payload = {
            'exp': expire_at,
            'iat': datetime.utcnow(),
            'data': data
        }
        return jwt.encode(payload, secret, algorithm=algo)
    except Exception as e:
        return e

def verify_auth_token(auth_token, secret, algo='HS256'):
    try:
        payload = jwt.decode(auth_token, secret, algorithms=algo)
        return payload
    except jwt.ExpiredSignatureError:
        return 'Token Expired'
    except jwt.InvalidTokenError:
        return 'Invalid Token'
