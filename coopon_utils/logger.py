import logging

DEFAULT_FORMATTER = logging.Formatter("[%(asctime)s] %(module)s: [%(levelname)s] %(message)s")

def get_logger(name, level=logging.INFO):
    _logger = logging.getLogger(name)
    _logger.setLevel(level)

    if not _logger.handlers:
        default_handler = logging.StreamHandler()
        default_handler.setFormatter(DEFAULT_FORMATTER)
        _logger.addHandler(default_handler)
    return _logger
