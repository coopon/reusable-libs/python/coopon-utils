import datetime
import os
import time
import unittest

from coopon_utils.crypto import (generate_unique_id, get_password_salt,
                                 verify_password, get_auth_token,
                                 verify_auth_token)


class CryptoTest(unittest.TestCase):
    def test_password_salt_and_verify_correct_password(self):
        plain_text = "mypassword123#"
        salted_pass = get_password_salt(plain_text)
        self.assertTrue(verify_password(plain_text, salted_pass))

    def test_password_salt_and_verify_incorrect_password(self):
        plain_text = "mypassword123#"
        salted_pass = get_password_salt(plain_text)
        self.assertFalse(verify_password("xyzw4232", salted_pass))

    def test_correct_auth_token(self):
        secret = os.urandom(24)
        expire_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=20)
        test_data = "testing"
        token = get_auth_token(test_data, secret, expire_at)
        decoded_data = verify_auth_token(token, secret)
        self.assertEqual(decoded_data['data'], test_data)

    def test_incorrect_auth_token(self):
        secret = os.urandom(24)
        expire_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=20)
        test_data = "testing"
        token = get_auth_token(test_data, secret, expire_at)
        random_token = 'sadfasdfasdfsdafsadfsdljzljfljsdfvfdoajoaldng'
        decoded_data = verify_auth_token(random_token, secret)
        self.assertEqual(decoded_data, "Invalid Token")

    def test_expired_auth_token(self):
        secret = os.urandom(24)
        expire_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=3)
        token = get_auth_token("testing", secret, expire_at)
        time.sleep(4)
        decoded_data = verify_auth_token(token, secret)
        self.assertEqual(decoded_data, 'Token Expired')
