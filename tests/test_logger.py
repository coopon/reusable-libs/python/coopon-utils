import logging
import unittest

from coopon_utils.logger import get_logger

class LoggerTest(unittest.TestCase):
    def test_default_get_logger(self):
        logger_name = "My Logger"
        log = get_logger(logger_name)
        self.assertIsInstance(log, logging.Logger)
        self.assertEqual(log.name, logger_name)
        self.assertEqual(log.level, logging.INFO)
        del log

    def test_get_logger_level(self):
        logger_name = "My Logger"
        log = get_logger(logger_name, level=logging.DEBUG)
        self.assertIsInstance(log, logging.Logger)
        self.assertEqual(log.name, logger_name)
        self.assertEqual(log.level, logging.DEBUG)
        del log
