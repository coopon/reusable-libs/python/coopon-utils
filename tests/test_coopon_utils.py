import unittest

from coopon_utils import remove_fields


class GenricUtilsTest(unittest.TestCase):
    def test_remove_fields(self):
        model_data = {"name": "something",
                      "password": "zzsasd",
                      "mobile": "9876543210"}
        fields = ["password", "mobile"]
        output = remove_fields(model_data, fields)
        for field in fields:
            self.assertNotIn(field, output)
